require 'net/ssh'
require 'puppet/util/network_device/juniper/facts'

class Puppet::Util::NetworkDevice::Juniper::Device

  attr_accessor :url, :transport

  def initialize(url, option = {})
    @url = URI.parse(url)
    @option = option

    Puppet.debug("Puppet::Device::Juniper: connecting to Juniper device #{@url.host}.")
    Net::SSH.start(@url.host, @url.user :password => @url.password)
  end

  def facts
    @facts ||= Puppet::Util::NetworkDevice::Juniper::Facts.new(@transport)
    facts = @facts.retreive

    facts
  end
end


