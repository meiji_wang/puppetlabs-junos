class Junos_parser

  require 'Nokogiri'
  attr_writer :xml_document, :xslt_document, :flat_format

  def transform()
      doc = Nokogiri::XML(File.read(@xml_document))
      xslt = Nokogiri::XSLT(File.read(@xslt_document))
      self.flat_format = xslt.transform(doc)
      puts @flat_format
  end #transform
end #class

filename= ARGV[0]
puts "Argument: #{filename}"

jp1 = Junos_parser.new
#jp1.xml_document = ARGV[0]
#jp1.xslt_document = ARGV[1]
jp1.xml_document = "C:/Users/wangm/Dropbox/WorkSpace/puppetlabs-junos/RubyModules/Sample-Reply-get-software-information.xml"
jp1.xslt_document = "C:/Users/wangm/Dropbox/WorkSpace/puppetlabs-junos/RubyModules/simple-software-information.xslt"

jp1.transform()
#puts jp1.flat_format
