<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="urn:ietf:params:xml:ns:netconf:base:1.0">

	<xsl:output method="text" />

    <xsl:template match="/">
    
      <xsl:value-of select="'re-name ='" />
      <xsl:value-of select="//a:re-name" />
      <xsl:value-of select="'&#10;'" /><!-- LF --> 
      
      <xsl:value-of select="'host-name =>'" />
      <xsl:value-of select="//a:host-name" />
      <xsl:value-of select="'&#10;'" /><!-- LF -->
      
      <xsl:for-each select ="a:rpc-reply/a:multi-routing-engine-results/a:multi-routing-engine-item/a:software-information/a:package-information" >
        <xsl:value-of select= "concat(./a:name, ' = ', ./a:comment)" />
        <xsl:value-of select="'&#10;'" /><!-- LF -->
        
      </xsl:for-each>


	</xsl:template>
</xsl:stylesheet>



