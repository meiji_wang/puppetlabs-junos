<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns::junos="http://xml.juniper.net/junos/11.1R1/junos"
  <xsl:template match="/" >
    <root>
    <xsl:for-each select="rpc-reply/multi-routing-engine-results/multi-routing-engine-item">
      <xsl:for-each select="a:software-information/a:package-information">
          <xsl:value-of select="name"/>
          <xsl:value-of select="comment"/>
      </xsl:for-each>
    </xsl:for-each>
    </root>
  </xsl:template>
</xsl:stylesheet> 

