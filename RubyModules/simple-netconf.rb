#!/usr/bin/env ruby

require 'rubygems'

class JUNOS
  require 'net/ssh'

  attr_writer :host, :user, :pwd, :mydata

  def send(xml)
    #session = Net::SSH.start(@host, @user, :password => @pwd, :timeout => 10, :verbose => :debug) do |session|
    session = Net::SSH.start(@host, @user, :password => @pwd, :timeout => 10 ) do |session|
      channel = session.open_channel do |channel|
        channel.subsystem("netconf") do |netconf, success|
          netconf.on_data do |netconf, data|
            puts "#{data}"
            #@mydata = data
            netconf.close if data === "]]>]]>"
          end
          netconf.on_close do |netconf|
            puts "(netconf) ON_CLOSE"
          end
          netconf.on_eof do |netconf|
            puts "(netconf) ON_EOF"
          end
          #netconf.send_data(IO.read('junos-hello.xml'))
          netconf.send_data(xml)
        end # netconf
        # puts "(netconf) ON_DATA: #{@mydata.inspect}"
      end #channel
    end #session
  end #send
end #class


filename= ARGV[0]
puts "Argument: #{filename}"

junos1 = JUNOS.new
junos1.host = '192.168.1.1'
junos1.user = 'root'
junos1.pwd = 'd3m04dm1n'
junos1.send(IO.read(filename))
