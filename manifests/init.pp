# Class: junos
#
# This module manages junos
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class junos {
  file { "${settings::confdir}/device":
    ensure => directory,
  }
}
