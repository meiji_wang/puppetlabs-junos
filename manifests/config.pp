define junos::config (
  $username = 'root',
  $password,
  $address,
  $target = "${settings::confdir}/device/${name}.conf"
) {

  file { $target:
    ensure => present,
    owner  => 'puppet',
    group  => 'puppet',
    mode   => '0644',
    content => template('junos/config.erb'),
  }

}
